package BaiTap;
import java.io.*;

import java.util.Scanner;

public class Bai2 {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		System.out.print("Input N= ");
		int n=scanner.nextInt();
		//tranfer to array type int
		int max=(n+"").length();
		int[]m=TranferToArray(max, n);

		Output(m,max);
		//calculation
		int difference=Cal(m,max);
		System.out.println("The difference= "+difference);
		//prime numbers < n
		System.out.println("Prime Numbers:");
		for(int j = 2; j<=n; j++)
			if(isPrimeNumber(j))
				System.out.print(j+" ");
	}



	public static boolean isPrimeNumber(int j){
		for(int i=2; i<j; i++)
			if(j%i == 0)
				return false;
		return true;
	}
	public static int [] TranferToArray(int max,int n)
	{
		int[]m=new int [max];
		for (int i = max-1; i >= 0; i--) {
			m[i]=n%10;
			n=n/10;
		}
		return m;
	}
	public static int Cal(int[] m,int max) 
	{ 
		int difference=m[0];
		for(int i=1;i<max;i++)
		{
			difference=difference-m[i];
		}
		return difference;
	} 
	public static void Output(int []m,int max)
	{
		for(int i=0;i<max;i++)
			System.out.print(m[i]+" ");
		System.out.println();
	}

}
